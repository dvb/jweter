package net.luminis.jweter

import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.Attributes

class SaxSampleHandler extends DefaultHandler {

    def list = []
    def count = 0
    int level = 0

    void startElement(String ns, String localName, String qName, Attributes atts) {

        switch (qName) {
            case 'httpSample':
                if (level == 0) {
                    def current = new SampleResult()
                    current.label = atts.getValue('lb')
                    current.timestamp = atts.getValue('ts') as long
                    current.time = atts.getValue('t') as long
                    current.success = atts.getValue('s') as boolean
                    current.responseCode = atts.getValue('rs')
                    list << current
                }
                level++
                break
        }
    }

    void characters(char[] chars, int offset, int length) {
        // nothing
    }

    void endElement(String ns, String localName, String qName) {

        switch (qName) {
           case 'httpSample':
               level--
               if (level == 0) {
                   count++
               }
               break
        }
    }
}
