package net.luminis.jweter

import javax.swing.JOptionPane
import org.xml.sax.SAXParseException
import javax.swing.SwingWorker

/**
 * SwingWorker for reading/parsing JMeter result (.jtl) files.
 */
class JtlFileReaderWorker extends SwingWorker {

    def layout
    JMeterResults resultsProcessor
    def frame
    String filename = ""
    boolean done = false

    JtlFileReaderWorker(String inputFile, def cardLayout, def layoutFrame) {
        filename = inputFile
        resultsProcessor = new JMeterResults()

        layout = cardLayout
        frame = layoutFrame
    }

    /**
     * @return   error message when an error occurred, null otherwise
     */
    String doInBackground() {
        resultsProcessor.file = new File(filename)
        try {
            resultsProcessor.parseJMeterResult(frame)
            done = true
        }
        catch (InterruptedIOException interrupt) {
            // Cancelled by user, do nothing
            return null
        }
        catch (SAXParseException parseError) {
            return "XML parsing error: " + parseError.message
        }
        catch (OutOfMemoryError oom) {
            return "The process ran out of memory. Restart with more memory or select smaller file."
        }
        catch (Throwable e) {
            return "An error occured: ${e}"
        }
    }

    void done() {
        if (done) {
            // Advance wizzard to next step
            layout.next(frame.contentPane)
        }
        else {
            def error = get()
            if (error)
                JOptionPane.showMessageDialog frame, error, "Error", JOptionPane.ERROR_MESSAGE
        }
    }
}
