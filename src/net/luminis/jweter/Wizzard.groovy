package net.luminis.jweter

import groovy.swing.SwingBuilder
import javax.swing.WindowConstants as WC
import java.awt.FlowLayout
import java.awt.BorderLayout
import javax.swing.JFileChooser
import javax.swing.JOptionPane
import java.util.prefs.Preferences
import javax.swing.DefaultComboBoxModel
import java.awt.Container
import java.awt.Component
import javax.swing.JPanel
import javax.swing.JButton
import java.awt.Color
import java.awt.Point
import javax.swing.JLabel

import javax.swing.border.EmptyBorder

class HooksPanel extends JPanel {
    String id
    SwingBuilder builder
    Closure enterHook
    Closure leaveHook

    void setEnterHook(Closure clos) {
        enterHook = clos
        //println "In setter: this is ${this}"
        enterHook.delegate = this
        enterHook.resolveStrategy = Closure.DELEGATE_ONLY
    }
}

class WizzardStep extends HooksPanel {
    public WizzardStep() {
        setLayout(new BorderLayout())
        def btmPanel = new JPanel()
        add(btmPanel, BorderLayout.SOUTH)
        btmPanel.add(new JButton("<<"))
        btmPanel.add(new JButton(">>"))
    }

}

class BetterCardLayout extends java.awt.CardLayout {

    public void next(Container parent) {
        def current = parent.components.find { it.visible }
        if (current instanceof HooksPanel) {
            if (current.leaveHook) {
                def result = current.leaveHook.call()
                if (result != null && result == false) {
                    // if leave hook explicitely return false (not null!), cancel the 'next'
                    return
                }
            }
        }
        super.next(parent)
        for (int i = 0; i < parent.getComponentCount(); i++) {
            Component comp = parent.getComponent(i);
            if (comp.isVisible()) {
                if (comp instanceof HooksPanel) {
                    if (comp.enterHook) {
                        comp.enterHook.call()
                    }
                }
            }
        }
    }
}

class Wizzard {

    String getVersion() {
        def versionTag = this.getClass().getResource("version.properties")?.readLines()?.getAt(0)
        def matcher = versionTag =~ /v([\d.]+)/
        matcher.matches() ? matcher[0][1]: ""
    }

    String getBuildNr() {
        def versionTag = this.getClass().getResource("version.properties")?.readLines()?.getAt(0)
        def matcher = versionTag =~ /v([\d.]+)(-(\d+)-\w+)/
        matcher.matches() ? "${matcher[0][1]}${matcher[0][2]}": ""
    }

    def swing = new SwingBuilder()
    def frame = swing.frame(title: 'JWeter', locationByPlatform: true, defaultCloseOperation: WC.EXIT_ON_CLOSE)
    
    def error(String message) {
        JOptionPane.showMessageDialog frame, message, "Error", JOptionPane.ERROR_MESSAGE
    }

    def execute(String fileArg) {
        def lru = new DefaultComboBoxModel(getLru().toArray())

        swing.actions
        {
            action(id: 'prev',
                    name: '<<',
                    closure: { layout.previous(frame.contentPane) })
            action(id: 'next',
                    name: '>>',
                    closure: { layout.next(frame.contentPane) })
        }

        swing.container(frame) {

            def resultsProcessor

            layout = cardLayout(new BetterCardLayout())
//    panel(new WizzardStep(), constraints: '0') {
//        panel(constraints: BorderLayout.CENTER) {
//            label 'Hi!'
//        }
//    }

            // Step 1: select JMeter results file
            panel(new HooksPanel(), constraints: '1') {
                borderLayout()
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Select JMeter results (.jtl) file '), parent: true)
                    tableLayout(cellpadding: 3) {
                        tr {
                            td { label 'File: ' }
                            td {
                                comboBox(id: 'inputFile', editable: true, model: lru, preferredSize: [300, 28],
                                        actionPerformed: { event ->
                                            if (event.actionCommand == "comboBoxChanged") {
                                                def file = new File(swing.inputFile.selectedItem)
                                                if (file.exists()) {
                                                    swing.fileSize.text = file.sizeAsReadableString
                                                }
                                                else {
                                                    swing.fileSize.text = ''
                                                    error "File '${swing.inputFile.selectedItem}' does not exist."
                                                }
                                            }
                                        })
                            }
                            td {
                                button(text: '...', actionPerformed: {
                                    def dir = new File(".")
                                    if (swing.inputFile.selectedItem) {
                                        dir = new File(swing.inputFile.selectedItem).parent
                                    }
                                    fc = new JFileChooser(dir)
                                    if (fc.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
                                        swing.fileSize.text = fc.selectedFile.sizeAsReadableString

                                        def selectedFile = fc.selectedFile.absolutePath
                                        swing.inputFile.addItem(selectedFile)
                                        swing.inputFile.setSelectedItem(selectedFile)
                                    }
                                })
                            }
                        }
                        tr {
                            td(colspan: 2) {
                                panel {
                                    flowLayout(hgap: 0)
                                    label(text: 'File size: ')
                                    label(id: 'fileSize', text: swing.inputFile.selectedItem? new File(swing.inputFile.selectedItem).sizeAsReadableString: '')   
                                }
                            }
                            td { }
                        }
                        tr {
                            td(colspan: 3) { vstrut(height: 100) }
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(text: '>>', actionPerformed: {
                        def inputFile = swing.inputFile.selectedItem
                        if (!new File(inputFile).isFile()) {
                            error 'Select a JMeter (.jtl) result file first.'
                        }
                        else {
                            addLru(inputFile)
                            def worker = new JtlFileReaderWorker(inputFile, layout, frame)
                            resultsProcessor = worker.resultsProcessor
                            worker.execute()
                        }
                    })
                }
            }

            // Step 2: show file info
            panel(new HooksPanel(id: "dizze"), constraints: '2',
                    enterHook: {
                        // Fill dialog with file details
                        builder.fileName.text = resultsProcessor.file as String
                        builder.fileInfoFileSize.text = resultsProcessor.file.sizeAsReadableString
                        builder.nrOfSamples.text = String.format("%,d", resultsProcessor.nrOfSamples)
                        builder.startTest.text = resultsProcessor.startTest.dateTimeString
                        builder.endTest.text = resultsProcessor.endTest.dateTimeString
                        builder.duration.text = resultsProcessor.duration as String
                        builder.fastest.text = resultsProcessor.fastest as String
                        builder.slowest.text = resultsProcessor.slowest as String
                    }) {
                borderLayout()
                current.builder = swing
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' JMeter test results '), parent: true)
                    gridLayout(cols: 1, rows: 1)    // This gridlayout will 'protect' the border (box takes more space than available)
                    panel(border:new EmptyBorder(0,10,0,10)) {
                        boxLayout()
                        panel {
                            gridLayout(cols: 1, rows: 0)
                            label(text: 'File:', horizontalAlignment: JLabel.RIGHT)
                            label('File size:', horizontalAlignment: JLabel.RIGHT)
                            label('Start of test:', horizontalAlignment: JLabel.RIGHT)
                            label('End of test:', horizontalAlignment: JLabel.RIGHT)
                            label('Number of samples:', horizontalAlignment: JLabel.RIGHT)
                            label('Test duration (seconds):', horizontalAlignment: JLabel.RIGHT)
                            label('Fastest response:', horizontalAlignment: JLabel.RIGHT)
                            label('Slowest response:', horizontalAlignment: JLabel.RIGHT)
                        }
                        hstrut(width: 10)
                        panel() {
                            gridLayout(cols: 1, rows: 0)
                            label(id: 'fileName')
                            label(id: 'fileInfoFileSize')
                            label(id: 'startTest')
                            label(id: 'endTest')
                            label(id: 'nrOfSamples')
                            label(id: 'duration')
                            label(id: 'fastest')
                            label(id: 'slowest')
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(action: prev)
                    button(action: next)
                }
            }

            // Step 3: select tags
            def selectList = []

            def defaultSelectValue = true
            panel(new HooksPanel(), constraints: '3',
                    enterHook: {
                        if (!selectList) {
                            resultsProcessor.labels.each { label ->
                                def minMaxValues = resultsProcessor.getCountMinMax(label)
                                selectList.add([defaultSelectValue, label] + minMaxValues)
                            }
                            builder.labelTable.invalidate();
                        }
                    },
                    leaveHook: {
                        selectList.each { resultsProcessor.selectLabel(it[1], it[0]) }
                        def any = selectList.any { it[0] }
                        if (!any) {
                            error("Select at least one tag...")
                        }
                        return any
                    })
                    {
                        current.builder = swing
                        borderLayout()
                        panel {
                            def bgColor = current.background
                            compoundBorder(outer: emptyBorder(13),
                                    inner: compoundBorder(outer: titledBorder(' Select the results to analyse '), inner: emptyBorder(13)), parent: true)
                            borderLayout()
                            scrollPane(id: 'scroller', constraints: BorderLayout.CENTER) {
                                current.viewport.background = Color.LIGHT_GRAY
                                table(id: 'labelTable', background: bgColor, preferredScrollableViewportSize: [454, 202]) {
                                    tableModel(list: selectList) {
                                        closureColumn(header: 'select', type: Boolean.class, preferredWidth: 50, read: { it[0] }, write: { list, value -> list[0] = value })
                                        closureColumn(header: 'name', preferredWidth: 300, read: { it[1] })
                                        closureColumn(header: '# samples', preferredWidth: 50, read: { it[2] })
                                        closureColumn(header: 'min', preferredWidth: 50, read: { it[3] })
                                        closureColumn(header: 'max', preferredWidth: 100, read: { it[4] })
                                    }
                                }
                            }
                            panel(constraints: BorderLayout.SOUTH) {
                                def tableModel = swing.labelTable.model
                                flowLayout(alignment: FlowLayout.LEFT, vgap: 3)
                                button(text: 'all', actionPerformed: {
                                    selectList.each { row -> row[0] = true };
                                    tableModel.fireTableDataChanged()
                                } )
                                tableModel.fireTableDataChanged()
                                button(text: 'none', actionPerformed: {
                                    selectList.each { row -> row[0] = false };
                                    tableModel.fireTableDataChanged()
                                } )
                            }
                        }
                        panel(constraints: BorderLayout.SOUTH) {
                            flowLayout().alignment = FlowLayout.RIGHT
                            button(action: prev)
                            button(action: next)
                        }
                    }

            // Step 4: select graph type.
            panel(new HooksPanel(), constraints: '4',
                    enterHook: { },
                    leaveHook: { })
                    {
                        current.builder = swing
                        borderLayout()
                        panel(constraints: BorderLayout.CENTER) {
                            compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Select graph type '), parent: true)
                            vbox {
                                buttonGroup().with { radioGroup ->
                                    hbox(alignmentX: Component.LEFT_ALIGNMENT,) {
                                        def rb1 = radioButton(id: '1', selected: true, buttonGroup: radioGroup)
                                        label(text: "Time series (scatter diagram)", mouseClicked: { rb1.selected = true })
                                    }
                                    hbox(alignmentX: Component.LEFT_ALIGNMENT) {
                                        def rb2 = radioButton(id: '2', buttonGroup: radioGroup)
                                        label(text: "Distribution Histogram", mouseClicked: { rb2.selected = true })
                                    }
                                }
                            }
                        }
                        panel(constraints: BorderLayout.SOUTH) {
                            flowLayout().alignment = FlowLayout.RIGHT
                            button(action: prev)
                            button(text: '>>', actionPerformed: {
                                if (swing.'1'.selected) {
                                    layout.show(frame.contentPane, '6')
                                }
                                else {
                                    layout.next(frame.contentPane)
                                }
                            })
                        }
                    }

            // Step 5: Histogram
            panel(new HooksPanel(), constraints: '5', enterHook: { builder.mmax.text = resultsProcessor.getMaxForSelectedLabels() }) {
                current.builder = swing
                borderLayout()
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Histogram options '), parent: true)
                    tableLayout(cellpadding: 3) {
                        tr {
                            td { label 'Min: ' }
                            td { min = textField(columns: 3, text: '0') }
                        }
                        tr {
                            td { label 'Max: ' }
                            td { max = textField(columns: 6, id: 'mmax') }
                        }
                        tr {
                            td { label 'Bins: ' }
                            td { bin = textField(text: '10', columns: 6, id: 'mbin') }
                        }
                        tr {
                            td { label 'Relative frequency: ' }
                            td { relative = checkBox() }
                        }
                        tr {
                            td { label 'All together: ' }
                            td { together = checkBox() }
                        }
                        tr {
                            td { }
                            td {
                                button(text: "graph", actionPerformed: {
                                    resultsProcessor.createHistogram(bin.text as int, min.text as int, max.text as int, relative.selected, together.selected) })
                            }
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(action: prev)
                    button(text: '>>', actionPerformed: {
                        resultsProcessor.createHistogram(bin.text as int, min.text as int, max.text as int, relative.selected, together.selected) })
                }
            }
            // Step 6: Scatterplot
            panel(constraints: "6") {
                borderLayout()
                def logAxisBtn
                def relativeTimeBtn
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Time series options '), parent: true)
                    vbox {
                        logAxisBtn = checkBox(text: 'use logarithmic axis')
                        relativeTimeBtn = checkBox(text: 'use relative time for X-axis')
                        button(text: "graph", actionPerformed: { resultsProcessor.createGraph(logAxisBtn.selected, relativeTimeBtn.selected) })
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(text: '<<', actionPerformed: { layout.show(frame.contentPane, '4') })
                    button(text: '>>', actionPerformed: { resultsProcessor.createGraph(logAxisBtn.selected, relativeTimeBtn.selected) })
                }
            }
        }

        frame.pack()
        frame.location = new Point(500, 200)
        frame.show()

        if (fileArg) {
            swing.inputFile.selectedItem = new File(fileArg).absolutePath
        }
    }

    List getLru() {
        def lru = Preferences.userNodeForPackage(this.class).node("lru")
        def count = lru.keys().size()
        count ? (0..count - 1).collect { lru.get(it as String, null) } : []
    }

    void addLru(String newItem) {

        def lru = Preferences.userNodeForPackage(this.class).node("lru")
        def count = lru.keys().size()
        def currentLruItems = count ? (0..count - 1).collect { lru.get(it as String, null) } : []

        def newLruItems = [newItem] + (currentLruItems - newItem)

        int max = 5
        newLruItems.eachWithIndex { value, index ->
            if (index < max) {
                lru.put(index as String, value)
            }
        }
    }

    void clearLru() {
        Preferences.userNodeForPackage(this.class).removeNode()
    }
}