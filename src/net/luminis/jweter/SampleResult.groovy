package net.luminis.jweter

class SampleResult {
    String label
    long timestamp
    int time
    boolean success
    String responseCode

    String toString() {
        "[${label}: ${timestamp}->${time}/${success}]"
    }
}

