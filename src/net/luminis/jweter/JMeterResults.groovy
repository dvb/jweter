package net.luminis.jweter

import org.jfree.data.xy.XYSeriesCollection
import org.jfree.data.xy.XYSeries
import org.jfree.chart.JFreeChart
import org.jfree.chart.ChartFactory
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.axis.LogarithmicAxis
import org.jfree.chart.ChartFrame
import javax.xml.parsers.SAXParserFactory
import org.xml.sax.InputSource
import org.jfree.data.statistics.HistogramDataset
import org.jfree.data.statistics.HistogramType
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer
import javax.swing.ProgressMonitorInputStream
import org.jfree.chart.axis.DateAxis
import org.jfree.chart.renderer.xy.StandardXYItemRenderer
import java.text.NumberFormat
import org.jfree.chart.labels.StandardXYToolTipGenerator

import java.text.SimpleDateFormat


class JMeterResults {

    File file
    List allResults = []
    long startTest = Long.MAX_VALUE
    long endTest = 0

    Set selectedLabels = [] as Set

    File getFile() {
        file
    }
    
    public void setFile(File file) {
        this.file = file
    }

    public int getNrOfSamples() {
        return allResults.size()
    }

    public Date getStartTest() {
        return new Date(startTest)
    }

    public Date getEndTest() {
        return new Date(endTest)
    }

    public long getDuration() {
        (endTest - startTest) / 1000
    }

    public Integer getFastest() {
        allResults.collect { it.time }.min()        
    }

    public Integer getSlowest() {
        allResults.collect { it.time }.max()
    }

    public Set getLabels() {
        Set tags = [] as Set
        allResults.each { tags.add(it.label) }
        tags
    }

    /**
     * Returns a list with: count (nr. samples), min and max for the indicated label
     */
    public List getCountMinMax(label) {
        def filtered = allResults.findAll{ result -> result.label == label }
        [ filtered.size(), filtered.time.min(), filtered.time.max()]
    }
    
    public int getMinForSelectedLabels() {

    }

    public Integer getMaxForSelectedLabels() {
        allResults.findAll { selectedLabels.contains(it.label) }.time.max()
    }

    public void selectLabel(String label, boolean on) {
        if (on) {
            selectedLabels.add(label)
            // println "On: ${label}"
        }
        else {
            selectedLabels.remove(label)
            // println "Off: ${label}"
        }
    }

    def parseJMeterResult(def frame) {
        parseWithSax(file, frame)
    }

    def parseWithXmlSlurper(File file, def listener) {

        def xml = new XmlSlurper().parse(file)

        // println "${new Date()}: got xml, start processing"

        xml.httpSample.each { sample ->

            SampleResult result = new SampleResult()
            result.label = sample.@lb.toString()
            result.timestamp = sample.@ts.toString() as long
            result.time = sample.@t.toString() as int
            result.success = sample.@s.toString() as boolean
            result.responseCode = sample.@rs.toString()
            allResults << result

            // Detect min and max time right away
            if (result.timestamp < this.@startTest) {   // Expliciet field access necessary because closure is (apparently) not in lexical scope of field
                startTest = result.timestamp
            }
            if (result.timestamp > this.@endTest) {
                endTest = result.timestamp
            }
        }
    }

    def parseWithSax(File file, def frame) {

        def start = System.currentTimeMillis()
        def handler = new SaxSampleHandler()
        def reader = SAXParserFactory.newInstance().newSAXParser().XMLReader
        reader.contentHandler = handler
        reader.errorHandler = handler

        def fileStream = new ProgressMonitorInputStream(frame, "Reading ${file}", new FileInputStream(file));
        def monitor = fileStream.progressMonitor
        def inputStream = new BufferedInputStream(fileStream)

        try {
            reader.parse(new InputSource(inputStream))
        }
        finally {
            monitor.close()
        }

        def end = System.currentTimeMillis()
        // println "parsed ${handler.list.size()} items in ${end - start} ms!"

        allResults = handler.list
        def timestamps = allResults.collect { it.timestamp }
        startTest = timestamps.min()
        endTest = timestamps.max()

        // println "computing min/max took: ${System.currentTimeMillis() - end}"
    }
    
    def createGraph(boolean logarithmicAxis, boolean relativeDomainAxis) {

        XYSeriesCollection c = new XYSeriesCollection()
        
        selectedLabels.each { currentTag ->
            def filtered = allResults.findAll { it.label == currentTag }
            // println "${filtered.size()} results for ${currentTag}"

            if (allResults.size() > 10000000) {
                println "filtering because of too much samples"
                def filtered2 = []
                filtered.eachWithIndex { it, index ->
                    if (index % 5 == 0)
                    filtered2 << it
                }
                println "from ${filtered.size()} to ${filtered2.size()}"
                filtered = filtered2
            }

            XYSeries series = new XYSeries(currentTag)
            filtered.each { sample ->
                if (relativeDomainAxis) {
                    series.add( (sample.timestamp - this.@startTest) / 1000, sample.time)
                }
                else {
                    series.add(sample.timestamp, sample.time)
                }
            }
            c.addSeries(series)
        }

        JFreeChart chart = ChartFactory.createScatterPlot(file.name,
                relativeDomainAxis? "time (seconds since start of test)": "time", "response time (ms)", c, PlotOrientation.VERTICAL, true, true, false)


        
        if (logarithmicAxis) {
            def axis = new LogarithmicAxis("response time (ms)")
            chart.getPlot().setRangeAxis(axis)
        }
        if (! relativeDomainAxis) {
            def axis = new DateAxis()
            chart.plot.domainAxis = new DateAxis()

            // Create a customized renderer in order to render the tooltips differently
            StandardXYToolTipGenerator tooltipGenerator = new StandardXYToolTipGenerator("{0}: {1}, {2}",
                    new SimpleDateFormat("HH:mm:ss"), NumberFormat.getInstance());
            StandardXYItemRenderer renderer = new StandardXYItemRenderer(StandardXYItemRenderer.SHAPES, tooltipGenerator, null);
            renderer.setShapesFilled(true);
            chart.plot.renderer = renderer;
        }
        ChartFrame chartFrame = new ChartFrame("JWeter - time series", chart)
        chartFrame.locationByPlatform = true
        chartFrame.pack()
        chartFrame.setSize(1050, 800)
        chartFrame.setVisible true
    }

    def createHistogram(int bins, int min, int max, boolean relative, boolean together) {
        HistogramDataset d = new HistogramDataset()
        if (relative) {
            d.type = HistogramType.RELATIVE_FREQUENCY
        }

        if (together) {
            def filtered = allResults.findAll { selectedLabels.contains(it.label) }
            def times = filtered.collect { it.time }
            def values = new double[filtered.size()]
            filtered.eachWithIndex { sample, idx -> values[idx] = sample.time }
            d.addSeries("request", values, bins, min, max)
        }
        else {
            selectedLabels.each { currentTag ->
                def filtered = allResults.findAll { it.label == currentTag }

                def times = filtered.collect { it.time }
                // println "For tag '${currentTag}' min = ${times.min()} and max = ${times.max()}"

                def values = new double[filtered.size()]
                filtered.eachWithIndex { sample, idx -> values[idx] = sample.time }

                d.addSeries(currentTag, values, bins, min, max)
            }
        }

        JFreeChart chart = ChartFactory.createHistogram(file.name, null, null, d, PlotOrientation.VERTICAL, true, true, false)
        def plot = chart.getPlot()
        if (selectedLabels.size() == 1) {
            plot.getRenderer().setMargin(0.2)
        }
        else {
            plot.setRenderer(new ClusteredXYBarRenderer(0.10, false))
        }

        ChartFrame chartFrame = new ChartFrame("JWeter - histogram", chart)
        chartFrame.locationByPlatform = true
        chartFrame.pack()
        chartFrame.setSize(1050, 800)
        chartFrame.setVisible true
    }
}

