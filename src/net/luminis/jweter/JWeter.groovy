package net.luminis.jweter

// Add method getSizeAsReadableString() to java.io.File
java.io.File.metaClass.getSizeAsReadableString = { 
    long size = delegate.length()
    int exponent = 0
    def relativeSize = size     // Intentionally using def: runtime type will change to float after division    
    while (relativeSize >= 1024) {
        relativeSize /= 1024
        exponent += 1
    }
    def unit
    switch(exponent) {
        case 0: unit = "bytes"; break;
        case 1: unit = "KB"; break;
        case 2: unit = "MB"; break;
        case 3: unit = "GB"; break;
        case 4: unit = "TB"; break;
        default: throw new Exception("file size to large to handle: ${size}")
    }
    def value = relativeSize as String
    value = value.length() > 4? value[0..3]: value   // Max 3 digits (and one decimal separator)
    if (value.endsWith("."))
        value = value[0..-2]
    value + ' ' + unit
}

// OSX specific initialization
System.setProperty("com.apple.mrj.application.apple.menu.about.name", "JWeter");

// Start wizzard and pass optional command line args.
def cli = new CliBuilder(usage: 'JWeter [-c|-v] [file]')
cli.with {
    c longOpt: 'clean',   'Remove all stored preferences'
    h longOpt: 'help',    'Show usage'
    v longOpt: 'version', 'Show version (and exit)'
}

def options = cli.parse(args)
if (options.arguments()?.getAt(0)?.startsWith('-')) {
    println "Invalid option '${options.arguments()[0]}'"
    cli.usage()
    return
}

def wizzard = new Wizzard()

if (options.h) {
    println "JWeter version ${wizzard.version}"
    cli.usage()
    return
}
if (options.v) {
    def version = wizzard.version
    if (version) {
        println "JWeter version ${version}"
    }
    else {
        println "Development release; build number ${wizzard.buildNr?:"unknown"}"
    }
    println "Author: Peter Doornbosch"
    return
}

if (options.c) {
    wizzard.clearLru()
}
wizzard.execute(options.arguments()? options.arguments()[0]: null)
